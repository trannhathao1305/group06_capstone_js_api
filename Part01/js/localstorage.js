const getLocalStorage = (key) => {
  const result = window.localStorage.getItem(key);
  return JSON.parse(result);
};

const setLocalStorage = (key, value) => {
  let data = JSON.stringify(value);
  window.localStorage.setItem(key, data);
};

// const deleteLocalStorage = (key) => {
//   window.localStorage.removeItem(key);
// };
