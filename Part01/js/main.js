const BASE_URL = "https://633ec05e0dbc3309f3bc54c2.mockapi.io";
const shoppingCart = document.getElementById("modal-body");
const cartQty = document.querySelector(".cart-qty");

const showCartBtn = document.querySelector(".cart");
const filterProductBtn = document.getElementById("mySelect");

// Events
showCartBtn.addEventListener("click", showProductCart);

if (!("groupBy" in Array.prototype)) {
  Array.prototype.groupBy = function (fn) {
    const keys = Object.keys(this[0]);
    const paramater = keys.find((key) => {
      return fn({ [key]: this[0][key] });
    });
    let filterValue = this.map((item) => {
      return item[paramater];
    });
    const arrFitlered = [...new Set(filterValue)].map((filterProp) => {
      return {
        [filterProp]: this.filter((item) => item[paramater] === filterProp),
      };
    });
    return arrFitlered.reduce((preValue, prop) => {
      return { ...preValue, ...prop };
    }, {});
  };
}

// get list from service
let fetchProductListService = function () {
  axios({
    url: `${BASE_URL}/qlsp`,
    method: "GET",
  }).then(async (res) => {
    const list = await renderProductList(res.data);
    await filterProduct(list);
  });
};
fetchProductListService();

// show product type
async function filterProduct(list) {
  filterProductBtn.addEventListener("change", (e) => {
    const productType = e.target.value;
    const productsFiltered = productType
      ? list.groupBy(({ type }) => type)[productType]
      : list;
    renderProductList(productsFiltered);
  });
}

// render product list
let renderProductList = async function (list) {
  let contentHTML = "";
  const productRender = list.reduce(function (preValue, product, index) {
    return (
      preValue +
      `
    <div class="products" id="product_${product.id}">
        <img src="${product.img}" alt="" class="product_img" />
        <h4 class="product_title">${product.name}</h4>
        <p>${product.desc}</p>
        <span class="price">$${product.price}</span>
        <i class="bx bx-shopping-bag add-cart"></i>
    </div>
    `
    );
  }, contentHTML);
  document.getElementById("productList").innerHTML = productRender;
  cartQty.innerHTML = getLocalStorage("CART")?.length || 0;

  // work with products
  addProductToCart(list);
  return await list;
};

// add product to cart
function addProductToCart(productList) {
  let list = [];
  let btnAddToCart = document.querySelectorAll(".add-cart");
  productList.forEach(function (item, index) {
    btnAddToCart[index].addEventListener("click", function () {
      let cart = getLocalStorage("CART") || list;
      const isExistProduct = cart.some((itemCart) => itemCart.id === item.id);
      if (!isExistProduct) {
        cart.push({ quantity: 1, ...item });
        setLocalStorage("CART", cart);
        cartQty.innerHTML = cart.length;
      }
    });
  });
}

// render product to cart
function renderProductCart(cart, totalPrice) {
  let contentHTML = "";
  cart.forEach(function (item) {
    totalPrice += Number(item.price * item.quantity);
    contentHTML += `
      <table class="table product-cart">
        <tr>
          <td>
            <img class="modal-img" src="${item.img}" />
          </td>
          <td>${item.name}</td>
          <td>
            <i class="bx bxs-left-arrow"></i>
            <span class="quantity">${item.quantity}</span>
            <i class="bx bxs-right-arrow"></i>
          </td>
          <td>$${item.price * item.quantity}</td>
          <td><i class="bx bxs-trash-alt"></i></td>
        </tr>
      </table>
      `;
  });
  return { total: totalPrice, content: contentHTML };
}

// update product in cart
function updateCartProduct(cart) {
  const btnMinus = document.querySelectorAll(".bxs-left-arrow");
  const btnPlus = document.querySelectorAll(".bxs-right-arrow");
  const btnDelete = document.querySelectorAll(".bxs-trash-alt");
  const btnRemoveAll = document.querySelector(".btn-danger");
  for (let i = 0; i < cart.length; i++) {
    let qty = cart[i].quantity;
    // reduce the number of products
    btnMinus[i].addEventListener("click", (e) => {
      if (qty > 1) {
        qty -= 1;
        cart[i]["quantity"] = qty;
        setLocalStorage("CART", cart);
      }
      showProductCart();
    });

    // increase the number of products
    btnPlus[i].addEventListener("click", (e) => {
      qty += 1;
      cart[i]["quantity"] = qty;
      setLocalStorage("CART", cart);
      showProductCart();
    });

    // remove a product
    btnDelete[i].addEventListener("click", (e) => {
      cart.splice(i, 1);
      setLocalStorage("CART", cart);
      showProductCart();
    });
  }
  // remove all product
  btnRemoveAll.addEventListener("click", (e) => {
    cart = [];
    setLocalStorage("CART", cart);
    showProductCart();
  });
}

//  Show product in cart
function showProductCart() {
  let getProductCart = getLocalStorage("CART") || [];
  cartQty.innerHTML = getProductCart?.length || 0;
  let totalPrice = 0;
  let totalPriceEl = document.getElementById("total-price");
  if (getProductCart.length) {
    const { total, content } = renderProductCart(getProductCart, totalPrice);
    totalPrice = total;
    // line show innerHTML Product
    shoppingCart.innerHTML = content;

    // work with cart
    updateCartProduct(getProductCart);
  } else {
    shoppingCart.innerHTML = "Giỏ hàng đang trống";
  }
  totalPriceEl.innerHTML = `Total: $${totalPrice}`;
}
