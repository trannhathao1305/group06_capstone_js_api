export class Product {
  constructor(
    id,
    name,
    price,
    avatar,
    screen,
    frontCamera,
    backCamera,
    type,
    description
  ) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.img = avatar;
    this.screen = screen;
    this.frontCamera = frontCamera;
    this.backCamera = backCamera;
    this.type = type;
    this.desc = description;
  }
}
// bật loading
export let batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
// tắt loading
export let tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};

// tắt nút thêm

export let tatAdd = function () {
  document.getElementById("btn-submit").classList.add("d-none");
};

// Bật nút thêm
export let batAdd = function () {
  document.getElementById("btn-submit").classList.add("d-block");
};

// Tắt nút cập nhật
export let tatUpdate = function () {
  document.getElementById("btn-capnhat").classList.add("d-none");
};

// Bật nút cập nhật

export let batUpdate = function () {
  document.getElementById("btn-capnhat").classList.add("d-block");
};
