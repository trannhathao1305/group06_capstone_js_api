let BASE_URL = "https://633ec05e83f50e9ba3b761d9.mockapi.io/";

// Lấy dữ liệu data từ back end về :
export let dataService = () => {
  return axios({
    url: `${BASE_URL}data-product`,
    method: "GET",
  });
};
// thêm dữ liệu
export let postDataService = (product) => {
  return axios({
    url: `${BASE_URL}data-product`,
    method: "POST",
    data: product,
  });
};

// Xóa dữ liệu
export let deleteDataService = (id) => {
  return axios({
    url: `${BASE_URL}data-product/${id}`,
    method: "DELETE",
  });
};

// Sửa dữ liệu
export let repairDataService = (id) => {
  return axios({
    url: `${BASE_URL}data-product/${id}`,
    method: "GET",
  });
};

// Cập nhật dữ liệu

export let updateDataService = (item) => {
  console.log(item);
  return axios({
    url: `${BASE_URL}data-product/${item.id}`,
    method: "PUT",
    data: item,
  });
};
