export let checkEmpty = (value, message) => {
  if (!value) {
    document.getElementById(message).innerHTML = "Vui lòng nhập trường này";
    return false;
  } else {
    document.getElementById(message).innerHTML = "";
    return true;
  }
};

export let checkText = (value, message) => {
  const regex = new RegExp("[A-zÀ-ÿ]");
  if (regex.test(value)) {
    document.getElementById(message).innerHTML = "";
    return true;
  } else {
    document.getElementById(message).innerHTML = "Trường này phải là chữ";
    return false;
  }
};

export let checkNumber = (value, message) => {
  const regex = /^[0-9]+$/;
  if (regex.test(value)) {
    document.getElementById(message).innerHTML = "";
    return true;
  } else {
    document.getElementById(message).innerHTML = "Trường này phải là số";
    return false;
  }
};

export let checkLinkImg = (value, message) => {
  const regex = /\.(jpeg|jpg|gif|png)$/;
  if (regex.test(value)) {
    document.getElementById(message).innerHTML = "";
    return true;
  } else {
    document.getElementById(message).innerHTML =
      "Trường này phải là link hình ảnh với các đuôi sau ( .JPEG/.JPG/.GIF/.PNG)";
    return false;
  }
};

// Kiểm tra trùng lặp account or id
export let duplicateCheck = (id, errorMessage, data) => {
  var index = data.findIndex(function (goods) {
    console.log(index);
    console.log(goods.id);
    return goods.id == id;
  });
  if (index == -1) {
    document.getElementById(errorMessage).innerHTML = "";
    return true;
  } else {
    document.getElementById(errorMessage).innerHTML = `ID đã tồn tại`;
    return false;
  }
};
